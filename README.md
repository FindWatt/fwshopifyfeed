# FwShopifyFeed

This is a converter that takes shopify feeds which are structured uniquely, and transforms them into a typical format of one product variant per row.

The shopify feed format has 1 or more rows per family id ("Handle").
Some of the fields only appear in the first row for the family (e.g. "Title", "Body (HTML)", "Vendor")
Some of the fields are for a specific combination of variant attributes (e.g. "Option1 Name", "Option1 Value", "Option2 Name", "Option2 Value")
Some of the fields are contain multiple values for a family, but aren't aligned with or particular to the variants (e.g. "Image Src", "Image Position")

Requires at least the columns "Handle", "Title", "Vendor", "Variant SKU" in order to be recognized as a shopify feed.

# Installation

```
pip install git+https://bitbucket.org/FindWatt/FwShopifyFeed
```


# Convert Spreadsheet Files using FwShopifyFeed.converter

This script can be run from the command line like "python converter.py path_to_shopify_feed.csv" or via a python call to convert_shopify_file.

The default family id column is "Handle".
The default columns that have multiple items for the family are "Image Src", "Image Position", "Image Alt Text".
The rest of the columns will be treated like parent/variant info with values filled down to any empty variant cells.


Usage:
```
#!python

import FwShopifyFeed

# convert file
s_output_file, pd_converted = FwShopifyFeed.convert_shopify_file(s_shopify_feed_file)

# convert pandas dataframe
pd_converted = FwShopifyFeed.convert_shopify_table(pd_shopify_feed)


```
From command line:
```
python converter.py path_to_shopify.csv

python converter.py path_to_shopify.csv "path_to_output.xlsx" False

python converter.py path_to_shopify.xlsx --output:"path_to_output.xlsx" --as_xlsx:True 

python converter.py path_to_shopify.csv --idx_col:"Handles" --unstack_cols:"Image Src" --unstack_cols:"Image Position"

```