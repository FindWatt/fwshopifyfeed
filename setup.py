import os, re, sys
import setuptools
from setuptools.command.install import install

module_path = os.path.join(os.path.dirname(__file__), 'FwShopifyFeed/__init__.py')
version_line = [line for line in open(module_path)
                if line.startswith('__version__')][0]

__version__ = re.findall("\d+\.\d+\.\d+", version_line)[0]


class CustomInstall(install):
    """
    @description: Installs external FindWatt dependencies before installing
    the current package.
    """
    def run(self):
        self.install_fw_dependencies()
        install.run(self)

    def install_fw_dependencies(self):
        if sys.version_info.major == 2:
            os.system("pip install git+https://bitbucket.org/FindWatt/fwfile")
            os.system("pip install git+https://bitbucket.org/FwPyClassification/fieldids")
        elif sys.version_info.major == 3:
            os.system("pip3 install git+https://bitbucket.org/FindWatt/fwfile")
            os.system("pip3 install git+https://bitbucket.org/FwPyClassification/fieldids")


setuptools.setup(
    name="FwShopifyFeed",
    version=__version__,
    url="https://bitbucket.org/FindWatt/fwshopifyfeed",

    author="FindWatt",

    description="Convert Shopify CSV feed into normal row-per-variant table.",
    long_description=open('README.md').read(),

    packages=setuptools.find_packages(),
    include_package_data=True,
    package_data={'': ["*.pyx"]},
    py_modules=['FwShopifyFeed'],
    zip_safe=False,
    platforms='any',

    install_requires=[
        "cython",
        "pandas>=0.25",
    ],
    cmdclass={'install': CustomInstall}
)
