﻿'''
Custom exceptions for parsing CSV files.
'''

class BrokenShopifyFeed(BaseException):
    pass

class ShopifyFeedNotSpecified(BrokenShopifyFeed):
    def __init__(self):
        message = "Shopify feed path not specified"
        super().__init__(message)

class ShopifyFeedMissing(BrokenShopifyFeed):
    def __init__(self, file_path):
        message = "Shopify feed is missing from path: ({})".format(file_path)
        super().__init__(message)

class NonValidShopifyFormat(BrokenShopifyFeed):
    def __init__(self, file_path):
        message = "Shopify feed is in a non-supported file format: ({})".format(file_path)
        super().__init__(message)

class NonShopifyFeed(BrokenShopifyFeed):
    def __init__(self, file_path):
        message = "Cannot convert file because it doesn't appear to be a Shopify feed with required columns: ({})".format(file_path)
        super().__init__(message)

class EmptyShopifyFeed(BrokenShopifyFeed):
    def __init__(self, file_path):
        message = "The Shopify feed is empty with no products to convert: ({})".format(file_path)
        super().__init__(message)
