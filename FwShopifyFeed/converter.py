﻿import os, re, sys
import pandas as pd
import FwFile
from FieldIDs import FIELD as a_FIELDS
from .exceptions import *

s_IDX_COL = "Handle"
a_IMG_COLS = ["Image Src", "Image Position", "Image Alt Text"]
a_REQ_COLS = ["Handle", "Title", "Vendor", "Variant SKU"]

def convert_shopify_table(pd_shopify_feed, idx_col=s_IDX_COL, unstack_cols=a_IMG_COLS):
    ''' Take a dataframe in the shopify format and convert it to a standard one-row-per-variant table
        Arguments:
            pd_shopify_feed: {pd.DataFrame} in shopify format
            idx_col: {str} the column name of the family id
            unstack_cols: {list} of columns that have multiple rows per family but aren't aligned with the variant cols
        Returns:
            {pd.DataFrame} with one-row-per-variant. Also has properties with counts of families, variants, and original shopify rows
    '''
    # index the feed by the family id column
    #pd_shopify_feed.set_index(idx_col, inplace=True)
    print(f"#{len(pd_shopify_feed)} rows")
    
    # get the pieces of information that are either per family or per variant
    pd_variants = pd_shopify_feed.copy().set_index(idx_col)
    unstack_cols = [col for col in unstack_cols if col in pd_variants.columns]
    if unstack_cols:
        pd_variants.drop(columns=unstack_cols, inplace=True)
        
    pd_variants = pd_variants.dropna(how='all').reset_index().groupby(idx_col, as_index=True).ffill().reset_index()
    #.apply(lambda df: df.drop(idx_col, axis=1, inplace=True))
    #print(pd_variants.head())
    #print(list(pd_variants.columns))
    
    print(f"#{len(pd_variants)} variants in #{len(set(pd_variants[idx_col]))} families")
    
    # transpose the family images into single rows indexed by family id and rename columns
    pd_images = pd_shopify_feed.groupby(idx_col).apply(lambda df: df.reset_index(drop=True))[unstack_cols].unstack()
    pd_images.columns = [
        "_".join(map(str, t_vals))
        for t_vals in zip(*[pd_images.columns.get_level_values(i) for i in range(pd_images.columns.nlevels)])]
    pd_images = pd_images.reset_index()
    #print(list(pd_images.columns))
    #print(f"#{len(pd_images)} family images")
    
    # append the family images onto the variant table and reset index
    pd_variants_with_images = pd.merge(
        pd_variants, #.drop(idx_col, axis=1).reset_index(),
        pd_images,
        on=idx_col,
        how='outer')
    
    pd_variants_with_images.rows = len(pd_shopify_feed)
    pd_variants_with_images.families = len(set(pd_variants_with_images[idx_col]))
    pd_variants_with_images.variants = len(pd_variants)
    
    return pd_variants_with_images


def convert_shopify_file(s_feed_path, output="", as_xlsx=False, **kwargs):
    ''' Take a shopify csv path and conver it to a regular FindWatt csv/xlsx
        Arguments:
            s_feed_path: {str} the path of the input shopify feed
            output: optional {str} of the output filename
            as_xlsx: {bool} write to xlsx instead of csv
            kwargs: {dict} of arguments to forward to convert_shopify_table
        Returns:
            {tuple} of (s_output_file_path, and pd_converted_feed)
    '''
    
    
    s_folder, s_file = os.path.split(s_feed_path)
    
    if not s_file:
        raise ShopifyFeedNotSpecified()
    
    # make sure specified input csv exists
    if not os.path.isfile(s_feed_path):
        raise ShopifyFeedMissing(s_feed_path)
        
    #print(f's_folder "{s_folder}"')
    #print(f's_file "{s_file}"')
    
    # make sure specified input design workbook is a csv
    if not s_feed_path.lower().endswith(("csv", "txt", "tsv", "tab", "xlsx")):
        raise NonValidShopifyFormat(s_feed_path)
    
    # figure out the output filename
    s_output_folder, s_output_file = os.path.split(output)
    #print("s_output_folder", s_output_folder)
    #print("s_output_file", s_output_file)
    
    if not s_output_folder:
        print("Write to input folder")
        s_output_folder = s_folder
    elif not os.path.isdir(s_output_folder):
        print(f'Creating output folder "{s_output_folder}"')
        assert FwFile.create_folder(os.path.join(s_output_folder, ""))
    
    if not s_output_file:
        s_output_file = FwFile.add_suffix_to_file(os.path.join(s_file), "_converted")
    if not s_output_file.lower().endswith(("csv", "txt", "tsv", "tab", "xlsx")):
        s_output_file = f"{s_output_file}.csv"
    
    if as_xlsx:
        s_output_file = re.sub(r"(csv|txt|tsv|tab)$", "xlsx", s_output_file, flags=re.I)
    else:
        s_output_file = re.sub(r"(xls|xlsx|xlsm)$", "csv", s_output_file, flags=re.I)
    s_output = os.path.join(s_output_folder, s_output_file)
    
    # read the file in
    if s_feed_path.lower().endswith(("csv", "txt", "tsv", "tab")):
        print(f'Converting Shopify CSV Feed "{s_feed_path}" to regular table.', FwFile.encoding_tester(s_feed_path, FwFile.detect_bom(s_feed_path)))
        pd_feed = pd.read_csv(s_feed_path, encoding=FwFile.encoding_tester(s_feed_path, FwFile.detect_bom(s_feed_path)), **kwargs)
    else:
        print(f'Converting Shopify Excel Feed "{s_feed_path}" to regular table.')
        pd_feed = pd.read_excel(s_feed_path, **kwargs)
    
    if len(pd_feed) == 0:
        raise EmptyShopifyFeed(s_feed_path)
    
    a_missing_required_cols = [col for col in a_REQ_COLS if col not in pd_feed.columns]
    if a_missing_required_cols:
        print("Missing required Shopify columns:", a_missing_required_cols)
        raise NonShopifyFeed(s_feed_path)
    
    # convert the file
    pd_converted = convert_shopify_table(pd_feed, **kwargs)
    
    # write the converted file
    print(f'Writing out to "{s_output}"')
    
    b_add_index = kwargs.get(
        "add_unique_id",
        not any([col.lower() in a_FIELDS['all']['unique_id'] for col in pd_converted.columns]))
    s_index_label = "Unique ID"
    #print('b_add_index', b_add_index)
    #print('s_index_label', s_index_label)
    
    if as_xlsx:
        pd_converted.to_excel(s_output, sheet_name="Shopify", index=b_add_index, index_label=s_index_label, **kwargs)
    else:
        pd_converted.to_csv(s_output, index=b_add_index, index_label=s_index_label, **kwargs)
        
    return s_output, pd_converted

def convert_command_line_arg(s_value, b_bool=True, b_int=True, b_float=True):
    ''' Convert the value of a str command line argument value into bool, int or float if appropriate '''
    if not isinstance(s_value, str): return s_value
    s_value = s_value.strip()
    if s_value.lower() in {'yes', 'true', 'on'}:
        if b_bool: return True
        else: return s_value
    elif s_value.lower() in {'no', 'false', 'off'}:
        if b_bool: return False
        else: return s_value
    elif b_int or b_float and re.search(r"\d", s_value) and not re.search(r"[a-zA-Z]", s_value):
        if b_int and re.match(r"^(\d{1,3},)*\d+$", s_value):
            return int(re.sub(r"[^\d]", "", s_value))
        elif b_float and re.match(r"^((\d{1,3},)*\d+(\.\d*)?|.\d+)$", s_value):
            return float(re.sub(r"[^\d.]", "", s_value))
        else:
            return s_value
    else:
        return s_value


if __name__ == "__main__":  
    ''' This gets run when converter.py is run from the command prompt
        Args:
            sys.argv: {list} of the command line options.
                [0] is the name of the script file
                [1] is the full or relative path to the shopify csv feed to convert
                [2] is the optional filename of the converted output file
                [3] is the optional boolean to write the converted file as xlsx instead of csv
    '''
    a_args, e_kwargs = [], {}
    
    idx = 1
    while idx < len(sys.argv):
        #print(idx, sys.argv[idx])
        if sys.argv[idx].startswith('--'):
            s_key = sys.argv[idx][2:]
            if ":" in s_key:
                s_key, val = s_key.split(':', 1)
                if s_key in e_kwargs:
                    if isinstance(e_kwargs[s_key], list):
                        e_kwargs[s_key].append(val)
                    else:
                        e_kwargs[s_key] = [e_kwargs[s_key], val]
                else:
                    e_kwargs[s_key] = val
            elif idx + 1 < len(sys.argv) and not sys.argv[idx + 1].startswith('--'):
                val = sys.argv[idx + 1]
                if s_key in e_kwargs:
                    if isinstance(e_kwargs[s_key], list):
                        e_kwargs[s_key].append(val)
                    else:
                        e_kwargs[s_key] = [e_kwargs[s_key], val]
                else:
                    e_kwargs[s_key] = sys.argv[idx + 1]
                idx += 1
            
            elif s_key not in e_kwargs:
                if ':' in s_key:
                    s_key, val = s_key.split(':', 1)
                else:
                    val = None
                
                if s_key in e_kwargs and val:
                    if isinstance(e_kwargs[s_key], list):
                        e_kwargs[s_key].append(val)
                    else:
                        e_kwargs[s_key] = [e_kwargs[s_key], val]
                else:
                    e_kwargs[s_key] = val
                
        else:
            a_args.append(convert_command_line_arg(sys.argv[idx]))
        idx += 1
    
    e_kwargs = {key.lower(): convert_command_line_arg(val) for key, val in e_kwargs.items()}
    print('a_args', a_args)
    print('e_kwargs', e_kwargs)
    
    if not a_args:
        print('Shopify Feed CSV is not specified')
        raise ShopifyFeedNotSpecified()
    
    elif a_args[0] is not None:
        s_shopify = a_args[0]
        t_path = os.path.split(s_shopify)
        
        if not t_path[0]:
        # if folder is not included, use the folder from script file
            s_folder = os.path.dirname(__file__)
            s_shopify = os.path.join(s_folder, s_shopify)
        
        if len(a_args) >= 2:
            if isinstance(a_args[1], bool):
                e_kwargs['as_xlsx'] = a_args[1]
            elif isinstance(a_args[1], str) and a_args[1].lower() in {'yes', 'true', 'on', 'overwrite', '1'}:
                e_kwargs['as_xlsx'] = True
            else:
                e_kwargs['output'] = a_args[1]
        if len(a_args) >= 3:
            if isinstance(a_args[2], bool):
                e_kwargs['as_xlsx'] = a_args[2]
            elif isinstance(a_args[2], str) and a_args[2].lower() in {'yes', 'true', 'on', 'overwrite', '1'}:
                e_kwargs['as_xlsx'] = True
            else:
                e_kwargs['output'] = a_args[2]
        
        convert_shopify_file(s_shopify, **e_kwargs)
