﻿import os
import pandas as pd
import FwFile
import FieldIDs
from FwShopifyFeed.converter import convert_shopify_file
from FwShopifyFeed.exceptions import *

s_INPUT_FOLDER = os.path.join(os.path.dirname(__file__), 'inputs')
s_OUTPUT_FOLDER = os.path.join(os.path.dirname(__file__), 'outputs')

a_TEST_GOOD_INPUTS = [
    ("products_export1.csv", None, 525, 79, 155),
    ("products_export2.csv", None, 525, 79, 155),
    ("products_export3.xlsx", None, 525, 79, 155),]
a_TEST_BAD_INPUTS = [
    ("", ShopifyFeedNotSpecified, 0, 0, 0),
    ("empty_export0.csv", ShopifyFeedMissing, 0, 0, 0),
    ("empty_export1.csv", EmptyShopifyFeed, 0, 0, 0),
    ("non_shopify1.csv", NonShopifyFeed, 0, 0, 0),
    ("non_shopify2.xlsb", NonValidShopifyFormat, 0, 0, 0),]
    

def print_versions():
    print(f'pandas: {pd.__version__}')
    print(f'FwFile: {FwFile.__version__}')
    print(f'FieldIDs: {FieldIDs.__version__}')

class TestShopifyConverter:
    def test_good_files(self):
        print_versions()
        for s_test_file, error, i_rows, i_families, i_variants in a_TEST_GOOD_INPUTS:
            s_output, pd_output = convert_shopify_file(
                s_feed_path=os.path.join(s_INPUT_FOLDER, s_test_file),
                output=s_OUTPUT_FOLDER,
            )
            #if s_output.lower().endswith(".xlsx"):
            #    pd_output = pd.read_excel(s_output)
            #else:
            #    pd_output = pd.read_csv(s_output)
            assert len(pd_output) == i_variants
            assert pd_output.rows == i_rows
            assert pd_output.families == i_families
            
            if s_output and os.path.isfile(s_output):
                os.remove(s_output)
            print('')

    def test_bad_files(self):
        print_versions()
        for s_test_file, error, i_rows, i_families, i_variants in a_TEST_BAD_INPUTS:
            try:
                s_output, pd_output = convert_shopify_file(
                    s_feed_path=os.path.join(s_INPUT_FOLDER, s_test_file),
                    output=s_OUTPUT_FOLDER,
                )
                if s_output and os.path.isfile(s_output):
                    os.remove(s_output)
            except error:
                pass
            print('')
